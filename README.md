# How to generate the librarie #
1. Open the terminal on project root directory and run **carthage update --platform ios**
2. Paste the **JWPlayer_iOS_SDK.framework** on the root directory
3. Now run the command on the terminal **carthage build --no-skip-current --platform ios**
4. Copy and past the builded frameworks that gonna be in Carthage/Build forlders

To run this framework you need to set go to Build Settings under "Target" and set "Allow Non-modular Includes in Framework Modules" to YES.

# How to build Example project #

### Carthage 
1. Open the terminal on project root directory and run **carthage update --platform ios && cd Example && carthage update --platform ios**
2. Download the **JWPlayer_iOS_SDK.framework** and past it into the the projects (Example folder and root folder)

### CocoaPods
1. Remove all the linked libraries in the adter and project but **YouboraJWPlayerAdapter.framework**
2. Run **pod install** on project root directory
