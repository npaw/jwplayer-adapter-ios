//
//  PlayerViewController.swift
//  JWPlayerSDKDemo
//
//  Created by Max Mikheyenko on 1/5/15.
//  Copyright (c) 2015 JWPlayer. All rights reserved.


import Foundation
import UIKit
import YouboraConfigUtils

class SwiftPlayerViewController: UIViewController, JWPlayerDelegate {
    let preRollAd = "https://playertest.longtailvideo.com/adtags/preroll_new.xml"
    
    let midRollPodAd = "https://playertest.longtailvideo.com/adtags/vast3_pod.xml"
    
    let midRollAd = "https://playertest.longtailvideo.com/adtags/vast_inline_nonlinear.xml"
    
    let postRollAd = "https://playertest.longtailvideo.com/adtags/vast_inline_nonlinear.xml"
    
    @objc public var containAds:Bool = false
    
    var player: JWPlayerController?
    
    @IBOutlet var callbacksView: UITextView!
    @IBOutlet var playerContainerView: UIView!
    
    var plugin: YBPlugin?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Swift Implementation"
        
        
        let options = YouboraConfigManager.getOptions()
        options.autoDetectBackground = true
        
        options.accountCode = "powerdev"
        
        self.plugin = YBPlugin(options: options)
        
        YBLog.setDebugLevel(.debug)
        
        self.configPlayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.embedPlayer()
        
        self.setupNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent || self.isBeingDismissed {
            self.plugin?.removeAdapter()
            self.plugin?.removeAdsAdapter()
            self.plugin = nil
        }
    }
    
    func configPlayer() {
        
        //MARK: JWConfig
        let config: JWConfig = JWConfig(contentURL: "http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8")
        config.controls = true
        
        self.configAds(config: config)
        
        self.player = JWPlayerController(config: config)
        
        
        
        if let player = player {
            let adapter = YBAdapterTransformer.transform(fromAdapter: YBJWPlayerAdapter(player: player))
            self.plugin?.adapter = adapter
            
            if (self.containAds) {
                let adsAdapter = YBAdapterTransformer.transform(fromAdapter: YBJWPlayerAdsAdapter(player: player))
                self.plugin?.adsAdapter = adsAdapter
            }
        }
    }
    
    func configAds(config: JWConfig) {
        if (!self.containAds) { return }
//
        // Create the Ad Breaks
        let adBreak = JWAdBreak(tag: midRollPodAd, offset: "pre")
        let adBreak2 = JWAdBreak(tag: midRollPodAd, offset: "10")
        let adBreak3 = JWAdBreak(tag: midRollAd, offset: "00:00:15:000")
        let adBreak4 = JWAdBreak(tag: midRollPodAd, offset: "25%")
        let adBreak5 = JWAdBreak(tag: postRollAd, offset: "post")
//
        // Create the AdConfig
        let adConfig = JWAdConfig()
        
        adConfig.schedule = [adBreak, adBreak2, adBreak3, adBreak4, adBreak5]

        // Initialize the JWConfig and create the JWPlayerController
        config.advertising = adConfig
    }
    
    func embedPlayer() {
        if let playerView = self.player?.view {
            playerView.frame = self.playerContainerView.frame
            self.playerContainerView.addSubview(playerView)
        }
    }
    
    @objc func playerStateChanged(_ info: Notification) {
        let userInfo: NSDictionary = (info as NSNotification).userInfo! as NSDictionary
        if( userInfo["event"] as! String == "onPause" ||
            userInfo["event"] as! String == "onReady" ||
            userInfo["event"] as! String == "onAdPause") {
            //self.playButton?.setTitle("Play", for: UIControl.State())
        } else {
            //self.playButton?.setTitle("Pause", for: UIControl.State())
        }
    }
    
    func setupNotifications() {
        let notifications = [
            JWPlayerStateChangedNotification,
            JWMetaDataAvailableNotification,
            JWAdActivityNotification,
            JWErrorNotification,
            JWCaptionsNotification,
            JWVideoQualityNotification,
            JWPlaybackPositionChangedNotification,
            JWFullScreenStateChangedNotification,
            JWAdClickNotification]
        
        let center:  NotificationCenter = NotificationCenter.default
        for(_, notification) in notifications.enumerated() {
            center.addObserver(self, selector: #selector(handleNotification(_:)), name: NSNotification.Name(rawValue: notification), object: nil)
        }
        
        center.addObserver(self, selector: #selector(playerStateChanged(_:)), name: NSNotification.Name(rawValue: JWPlayerStateChangedNotification), object: nil)
        center.addObserver(self, selector: #selector(playerStateChanged(_:)), name: NSNotification.Name(rawValue: JWAdActivityNotification), object: nil)
    }
    
    @objc func handleNotification(_ notification: Notification) {
        let userInfo: Dictionary = (notification as NSNotification).userInfo!
        let callback: String = userInfo["event"] as! String
        
        if(callback == "onTime") {return}
        
        var text: String = self.callbacksView!.text
        text = text.appendingFormat("\n%@", callback)
        self.callbacksView?.text = text
    }
}
