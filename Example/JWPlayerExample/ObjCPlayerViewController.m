//
//  PlayerViewController.m
//  JWPlayerSDKDemo
//
//  Created by Max Mikheyenko on 1/5/15.
//  Copyright (c) 2015 JWPlayer. All rights reserved.
//

#import "ObjCPlayerViewController.h"
#import <YouboraJWPlayerAdapter/YouboraJWPlayerAdapter.h>
#import <JWPlayer_iOS_SDK/JWPlayerController.h>
@import YouboraLib;
@import YouboraConfigUtils;


#define preRollAd "https://playertest.longtailvideo.com/adtags/preroll_new.xml"
#define midRollPodAd "https://playertest.longtailvideo.com/adtags/vast3_pod.xml"
#define midRollAd "https://playertest.longtailvideo.com/adtags/vast_inline_nonlinear.xml"
#define postRollAd "https://playertest.longtailvideo.com/adtags/vast_inline_nonlinear.xml"

@interface ObjCPlayerViewController () <JWPlayerDelegate>

@property JWPlayerController *player;
@property YBPlugin *plugin;

@property (nonatomic) IBOutlet UITextView *callbacksView;
@property (nonatomic) IBOutlet UIView *playerContainerView;
@end

@implementation ObjCPlayerViewController

#pragma mark - view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Objective C Implementation";
    
    YBOptions *options = [YouboraConfigManager getOptions];
    
    options.waitForMetadata = false;
    options.offline = NO;
    options.autoDetectBackground = true;
    options.accountCode = @"powerdev";
    
    self.plugin = [[YBPlugin alloc] initWithOptions:options];
    
    [YBLog setDebugLevel:YBLogLevelVerbose];
    
    [self configPlayer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self embedPlayer];
    [self setupNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([self isMovingFromParentViewController] || [self isBeingDismissed]) {
        if (self.plugin) {
            [self.plugin removeAdapter];
            [self.plugin removeAdsAdapter];
            self.plugin = nil;
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - player creation
-(void)configPlayer {
    JWConfig *config = [JWConfig configWithContentURL:@"http://qthttp.apple.com.edgesuite.net/1010qwoeiuryfg/sl.m3u8"];
    config.controls = YES;
    
    [self configAds:config];
    
    self.player = [[JWPlayerController alloc] initWithConfig:config];
    
    if (self.plugin && self.player) {
        [self.plugin setAdapter:[[YBJWPlayerAdapter alloc] initWithPlayer:self.player]];
        
        if (self.containAds) {
            [self.plugin setAdsAdapter:[[YBJWPlayerAdsAdapter alloc] initWithPlayer:self.player]];
        }
    }
    
}

-(void)configAds:(JWConfig*)config {
    if (!self.containAds) { return; }
    
    JWAdBreak *adBreak = [[JWAdBreak alloc] initWithTags:@[@midRollPodAd] offset:@"pre"];
    JWAdBreak *adBreak2 = [[JWAdBreak alloc] initWithTags:@[@midRollPodAd] offset:@"10"];
    JWAdBreak *adBreak3 = [[JWAdBreak alloc] initWithTags:@[@midRollAd] offset:@"00:00:15:000"];
    JWAdBreak *adBreak4 = [[JWAdBreak alloc] initWithTags:@[@midRollPodAd] offset:@"25%"];
    JWAdBreak *adBreak5 = [[JWAdBreak alloc] initWithTags:@[@postRollAd] offset:@"post"];
    
    JWAdConfig *adConfig = [[JWAdConfig alloc] init];
    
    adConfig.schedule = @[adBreak, adBreak2, adBreak3, adBreak4, adBreak5];
    
    // Initialize the JWConfig and create the JWPlayerController
    config.advertising = adConfig;
}

-(void)embedPlayer {
    if (self.player.view) {
        self.player.view.frame = self.playerContainerView.frame;
        [self.playerContainerView addSubview:self.player.view];
    }
}

#pragma mark - callbacks

- (void)setupNotifications
{
    NSArray *notifications = @[JWPlayerStateChangedNotification, JWMetaDataAvailableNotification, JWAdActivityNotification, JWErrorNotification, JWCaptionsNotification, JWVideoQualityNotification, JWPlaybackPositionChangedNotification, JWFullScreenStateChangedNotification, JWAdClickNotification];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [notifications enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [center addObserver:self selector:@selector(handleNotification:) name:obj object:nil];
    }];
}

#pragma mark - callback notification handling

- (void)handleNotification:(NSNotification*)notificaiton
{
    NSDictionary *userInfo = notificaiton.userInfo;
    NSString *callback = userInfo[@"event"];
    
    if([callback isEqualToString:@"onTime"]) { return;}
    
    NSString *text = self.callbacksView.text;
    text = [text stringByAppendingFormat:@"\n%@ %@",callback, userInfo];
    self.callbacksView.text = text;
}

@end
