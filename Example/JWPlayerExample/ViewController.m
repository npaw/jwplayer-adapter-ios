//
//  ViewController.m
//  JWPlayer-Developer-Demo
//
//  Created by Max Mikheyenko on 1/7/15.
//  Copyright (c) 2015 JWPlayer. All rights reserved.
//

#import "ViewController.h"
#import "ObjCPlayerViewController.h"
#import "JWPlayerExample-Swift.h"
@import YouboraConfigUtils;

@import YouboraConfigUtils;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonYouboraCfg;
@property (weak, nonatomic) IBOutlet UISwitch *includeAds;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"JW Player Developer Guide";
}

-(IBAction)pressToObjc:(id)sender {
    UIViewController *playerController = [self.storyboard instantiateViewControllerWithIdentifier:@"ObjCPlayerViewController"];
    
    if ([playerController isKindOfClass:[ObjCPlayerViewController class]]) {
        ((ObjCPlayerViewController*)playerController).containAds = self.includeAds.on;
    }
    [self showViewController:playerController];
}

-(IBAction)pressToSwift:(id)sender {
    UIViewController *playerController = [self.storyboard instantiateViewControllerWithIdentifier:@"SwiftPlayerViewController"];
    
    if ([playerController isKindOfClass:[SwiftPlayerViewController class]]) {
        
        ((SwiftPlayerViewController*)playerController).containAds = self.includeAds.on;
    }
    
    [self showViewController:playerController];
}

-(IBAction)pressAtSettings:(id)sender {
    [self showViewController:[YouboraConfigViewController initFromXIBWithAnimatedNavigation:true]];
}

-(void)showViewController:(UIViewController*)viewController {
   if (self.navigationController) {
        [self.navigationController showViewController:viewController sender:nil];
    } else {
        [self presentViewController:viewController animated:true completion:nil];
    }
}

@end
