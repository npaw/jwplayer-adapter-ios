## [6.6.0] - 2022-01-26
### Added
- YouboraLib 6.6.x compatibility

## [6.5.2] - 2020-08-14
### Fixed
- Fix ad resource to use max 1000 characters
- Don't fire ads when in adapter is stopped

### Added
- Name to ads adapter
- AdBreakStop to ads adapter

## [6.5.1] - 2020-06-08
### Fixed 
- Imports to the new librarie updates
- Carthage instalation

## [6.5.0] - 2019-07-25
### Added
- New ad events and properties

## [6.0.1] - 2018-10-01
### Fixed
 - Minor bugfixing

## [6.0.0] - 2018-08-10
### Release
 - Release version
