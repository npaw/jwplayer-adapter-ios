Pod::Spec.new do |s|

  s.name         = 'YouboraJWPlayerAdapter'
  s.version      = '6.6.0'

  # Metadata
  s.summary      = 'Adapter to use YouboraLib on JWPlayer'

  s.description  = <<-DESC
                        YouboraJWPlayerAdapter is an adapter used for JWPlayer.
                      DESC

  s.homepage     = 'http://developer.nicepeopleatwork.com/'

  s.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  s.author             = { 'NPAW' => 'support@nicepeopleatwork.com' }

  # Platforms
  s.ios.deployment_target = '9.0'
  #s.tvos.deployment_target = '9.0'

  s.swift_version = '5.0'

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/jwplayer-adapter-ios.git', :tag => s.version }

  # Source files
  s.source_files  = 'YouboraJWPlayerAdapter/**/*.{h,m}'
  s.public_header_files = 'YouboraJWPlayerAdapter/**/*.h'

  # Project settings
  s.requires_arc = true
  s.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORAADAPTER_VERSION=' + s.version.to_s }

  # Dependency
  s.dependency 'YouboraLib', '~> 6.5'
  s.dependency 'JWPlayer-SDK', '~> 3.0'

end
