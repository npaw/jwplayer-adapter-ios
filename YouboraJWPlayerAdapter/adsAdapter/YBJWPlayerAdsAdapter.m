//
//  YBJWAdsAdapter.m
//  YouboraJWPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 20/10/2017.
//  Copyright © 2017 NPAW. All rights reserved.
//

#import "YBJWPlayerAdsAdapter.h"

// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORAADAPTER_VERSION)
#define PLUGIN_NAME_DEF "JWPlayer-Ads"
#define PLUGIN_PLATFORM_DEF "iOS"

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF

#define MAX_RESOURCE_SIZE 1000

// Ads
NSString * const YBJWPlayerEventAdRequest = @"onAdRequest";
NSString * const YBJWPlayerEventAdPause = @"onAdPause";
NSString * const YBJWPlayerEventAdPlay = @"onAdPlay";
NSString * const YBJWPlayerEventAdComplete = @"onAdComplete";
NSString * const YBJWPlayerEventAdSkipped = @"onAdSkipped";
NSString * const YBJWPlayerEventAdError = @"onAdError";

NSString * const YBJWPlayerEventKeyAdTag = @"tag";
NSString * const YBJWPlayerEventKeyAdPosition = @"adposition"; // pre, mid, post
NSString * const YBJWPlayerEventKeyPosition = @"position"; // playhead
NSString * const YBJWPlayerEventKeyDuration = @"duration";
NSString * const YBJWPlayerEventAdBreakStart = @"onAdBreakStart";
NSString * const YBJWPlayerEventAdBreakStop = @"onAdBreakEnd";
NSString * const YBJWPlayerEventAdImpression = @"onAdImpression";

@interface YBJWPlayerAdsAdapter()
    
    // Some info can't be obtained at any time. Instead, the player reports things
    // such as playhead or ad position periodically, so we store those values
    // in order to return them when the lib asks for them.
    @property (nonatomic, assign) double lastReportedAdPlayhead;
    @property (nonatomic, assign) double lastReportedAdDuration;
    @property (nonatomic, assign) YBAdPosition lastReportedAdPosition;
    @property (nonatomic, strong) NSString * lastReportedAdTag;

    @property (nonatomic, assign) BOOL firstQuartileFired;
    @property (nonatomic, assign) BOOL secondQuartileFired;
    @property (nonatomic, assign) BOOL thirdQuartileFired;
    
    @end

@implementation YBJWPlayerAdsAdapter
    
- (void)registerListeners {
    [super registerListeners];
    
    [self resetValues];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adActivity:) name:JWAdActivityNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adProgress:) name:JWAdPlaybackProgressNotification object:nil];
    
    //TODO Adnalyzer
    
    if(self.plugin.adsAdapter != nil){
        [self.plugin.adsAdapter fireStop];
    }
}
    
- (void) unregisterListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super unregisterListeners];
}
    
#pragma mark - Notifications
- (void) adActivity:(NSNotification*)notification {
    NSString * evt = notification.userInfo[@"event"];
    [YBLog debug:@"AD EVENT: %@", evt];
    
    if ([evt isEqualToString:YBJWPlayerEventAdRequest]) {
        //[YBLog debug:@"AD EVENT: %@", evt];
        // should be ad start actually...
        NSString * adPosition = notification.userInfo[YBJWPlayerEventKeyAdPosition];
        self.lastReportedAdPosition = YBAdPositionMid;
        if (adPosition != nil && ![adPosition isEqualToString:@""]) {
            if([adPosition isEqualToString:@"pre"]){
                self.lastReportedAdPosition = YBAdPositionPre;
            }
            if([adPosition isEqualToString:@"mid"]){
                self.lastReportedAdPosition = YBAdPositionMid;
            }
            if([adPosition isEqualToString:@"post"]){
                self.lastReportedAdPosition = YBAdPositionPost;
            }
        }
        NSString * adTag = notification.userInfo[YBJWPlayerEventKeyAdTag];
        if (adTag != nil && ![adTag isEqualToString:@""]) {
            self.lastReportedAdTag = adTag;
        }
        [self fireAdManifest:nil];
    } else if ([evt isEqualToString:YBJWPlayerEventAdImpression]) {
        [self fireAdBreakStart];
        [self fireJoin];
        [self fireStart];
    } else if ([evt isEqualToString:YBJWPlayerEventAdComplete]){
        [YBLog debug:evt];
        [self fireStop];
        [self resetValues];
    } else if ([evt isEqualToString:YBJWPlayerEventAdPlay]) {
        [YBLog debug:evt];
        NSString * adTag = notification.userInfo[YBJWPlayerEventKeyAdTag];
        if (adTag != nil && ![adTag isEqualToString:@""]) {
            self.lastReportedAdTag = adTag;
        }
        //[self fireJoin];
        [self fireResume];
    } else if ([evt isEqualToString:YBJWPlayerEventAdSkipped]){
        [YBLog debug:evt];
        [self fireStop:@{@"skipped":@"true"}];
        [self resetValues];
    } else if ([evt isEqualToString:YBJWPlayerEventAdPause]){
        [YBLog debug:evt];
        [self firePause];
    } else if ([evt isEqualToString:YBJWPlayerEventAdError]){
        [YBLog debug:evt];
        NSString *message = notification.userInfo[@"message"];
        NSString *code = [notification.userInfo[@"code"] stringValue];
        [self fireErrorWithMessage:message code:code andMetadata:nil];
    } else if ([evt isEqualToString:YBJWPlayerEventAdBreakStart]) {
        [self fireAdBreakStart];
    } else if ([evt isEqualToString:YBJWPlayerEventAdBreakStop]) {
        [self fireAdBreakStop];
    }
}

- (void) adProgress:(NSNotification*)notification {
    
    // JWPlayer's "position" is Youbora's "playhead"
    double playhead = [notification.userInfo[YBJWPlayerEventKeyPosition] doubleValue];
    
    if (playhead > 0) {
        self.lastReportedAdPlayhead = playhead;
    }
    
    double duration = [notification.userInfo[YBJWPlayerEventKeyDuration] doubleValue];
    
    if (duration > 0) {
        self.lastReportedAdDuration = duration;
    }
    
    if (playhead > duration * 0.25 && self.firstQuartileFired == false) {
        [self fireQuartile:1];
        self.firstQuartileFired = true;
    }
    
    if (playhead > duration * 0.50 && self.secondQuartileFired == false) {
        [self fireQuartile:2];
        self.secondQuartileFired = true;
    }
    
    if (playhead > duration * 0.75 && self.thirdQuartileFired == false) {
        [self fireQuartile:3];
        self.thirdQuartileFired = true;
    }
}
    
#pragma mark - Private methods
    
- (void) resetValues {
    self.lastReportedAdPlayhead = 0;
    self.lastReportedAdDuration = 0;
    self.lastReportedAdPosition = [super getPosition];
    self.lastReportedAdTag = [super getResource];
    self.firstQuartileFired = false;
    self.secondQuartileFired = false;
    self.thirdQuartileFired = false;
}
    
#pragma mark - Info methods

- (NSString *)getPlayerVersion{
    return [NSString stringWithFormat:@"%@-%@", @PLUGIN_NAME_DEF, self.player.playerVersion];
}
    
-(NSString*)getPlayerName{
    return @PLUGIN_NAME_DEF;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

- (NSNumber *)getPlayhead {
    return @(self.lastReportedAdPlayhead);
}
    
- (YBAdPosition)getPosition {
    return self.lastReportedAdPosition;
}
    
- (NSNumber *)getDuration {
    return @(self.lastReportedAdDuration);
}
    
- (NSString *)getResource {
    if ([self.lastReportedAdTag length] <= MAX_RESOURCE_SIZE) {
        return self.lastReportedAdTag;
    }
    
    return [self.lastReportedAdTag substringWithRange:NSMakeRange(0, MAX_RESOURCE_SIZE)];
}
    
- (void) fireStart {
    [super fireStart];
}

@end
