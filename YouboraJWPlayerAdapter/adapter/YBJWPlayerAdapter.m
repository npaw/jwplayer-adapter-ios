//
//  YBJWPlayerAdapter.m
//  YouboraJWPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 20/10/2017.
//  Copyright © 2017 NPAW. All rights reserved.
//

#import "YBJWPlayerAdapter.h"
#import "YBJWPlayerAdsAdapter.h"


// Constants
#define MACRO_NAME(f) #f
#define MACRO_VALUE(f) MACRO_NAME(f)

#define PLUGIN_VERSION_DEF MACRO_VALUE(YOUBORAADAPTER_VERSION)
#define PLUGIN_NAME_DEF "JWPlayer"
#define PLUGIN_PLATFORM_DEF "iOS"

#define PLUGIN_NAME @PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF
#define PLUGIN_VERSION @PLUGIN_VERSION_DEF "-" PLUGIN_NAME_DEF "-" PLUGIN_PLATFORM_DEF

// Constants for JWPlayer events
NSString * const YBJWPlayerEventKey = @"event";

// Content
NSString * const YBJWPlayerEventFirstFrame = @"onFirstFrame";
NSString * const YBJWPlayerEventPlayAttempt = @"onPlayAttempt";
NSString * const YBJWPlayerEventPlay = @"onPlay";
NSString * const YBJWPlayerEventBeforePlay = @"onBeforePlay";
NSString * const YBJWPlayerEventIdle = @"onIdle";
NSString * const YBJWPlayerEventBuffer = @"onBuffer";
NSString * const YBJWPlayerEventPause = @"onPause";
NSString * const YBJWPlayerEventComplete = @"onComplete";
NSString * const YBJWPlayerEventTime = @"onTime";
NSString * const YBJWPlayerEventSeek = @"onSeek";
NSString * const YBJWPlayerEventError = @"onError";

@interface YBJWPlayerAdapter()
    
    @property (nonatomic, strong) NSString * lastReportedTitle;
    @property (nonatomic, strong) NSString * lastReportedResource;
    @property (nonatomic, strong) NSNumber * lastreportedPlayhead;
    
    // Properties to control seek closing event
    @property (nonatomic, assign) bool seekedRecently;
    @property (nonatomic, assign) double seekedFrom;
    @property (nonatomic, assign) double seekedTo;
    
    @property (nonatomic, strong) NSNumber * indicatedBitrate;
    @property (nonatomic, strong) NSNumber * observedBitrate;
    @property (nonatomic, strong) NSString * rendition;
    
    @end

@implementation YBJWPlayerAdapter
    
- (void)registerListeners {
    [super registerListeners];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(onMeta:) name:JWMetaDataAvailableNotification object:nil];
    [center addObserver:self selector:@selector(playerError:) name:JWErrorNotification object:nil];
    [center addObserver:self selector:@selector(updatePlaybackTimer:) name:JWPlaybackPositionChangedNotification object:nil];
    [center addObserver:self selector:@selector(playerStateChanged:) name:JWPlayerStateChangedNotification object:nil];
    [center addObserver:self selector:@selector(adActivity:) name:JWAdActivityNotification object:nil];
    [center addObserver:self selector:@selector(playlistNotification:) name:JWPlaylistNotification object:nil];
    
    if(self.plugin.adsAdapter != nil){
        [self.plugin.adsAdapter fireStop];
    }
}
    
- (void) unregisterListeners {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if(self.plugin != nil && self.plugin.adsAdapter != nil && [self.plugin.adsAdapter isKindOfClass:[YBJWPlayerAdsAdapter class]]){
        [self.plugin removeAdsAdapter];
    }
    
    [super unregisterListeners];
}
    
#pragma mark - Notification callbacks
- (void) playlistNotification:(NSNotification*)notification {
    
    NSString * evt = notification.userInfo[YBJWPlayerEventKey];
    
    [YBLog debug:@"playlistNotification: %@", evt];
    
    if ([evt isEqualToString:@"onPlaylistItem"]) {
        
        [self fireStop];
        [self resetValues];
        
        // Save title and resource
        NSDictionary * item = notification.userInfo[@"item"];
        NSString * title = item[@"title"];
        NSString * resource = item[@"file"];
        
        [YBLog debug:@"Resource from onPlaylistItem: %@", resource];
        
        if (title) {
            self.lastReportedTitle = title;
        }
        
        if (resource) {
            self.lastReportedResource = resource;
        }
        if(!self.flags.started){
            [self fireStart];
        }
    } else if ([evt isEqualToString:@"onPlaylistComplete"]) {
        [self fireStop];
        [self resetValues];
    }
}
    
- (void) onMeta:(NSNotification *) notification {
    NSDictionary * userInfo = notification.userInfo;
    
    NSNumber * br = userInfo[@"metadata"][@"observedBitrate"];
    if (br != nil) {
        self.observedBitrate = br;
    }
    
    br = userInfo[@"metadata"][@"indicatedBitrate"];
    if (br != nil) {
        self.indicatedBitrate = br;
        self.rendition = [YBYouboraUtils buildRenditionStringWithWidth:0 height:0 andBitrate:br.doubleValue];
    }
    
    [YBLog debug:@"onMeta"];
}
    
- (NSNumber *)getBitrate {
    return self.indicatedBitrate;
}
    
- (NSNumber *)getThroughput {
    return self.observedBitrate;
}
    
- (NSString *)getRendition {
    return self.rendition;
}
    
- (void) playerError:(NSNotification*)notification {
    if ([notification.userInfo[YBJWPlayerEventKey] isEqualToString:YBJWPlayerEventError]) {
        // The player does not report anything about the error...
        [self fireFatalErrorWithMessage:nil code:nil andMetadata:nil];
        [self resetValues];
    }
}
    
- (void)updatePlaybackTimer:(NSNotification*)notification {
    
    NSString * evt = notification.userInfo[YBJWPlayerEventKey];
    if ([evt isEqualToString:YBJWPlayerEventSeek]) {
        [YBLog debug:@"seek from: %@, to: %@", notification.userInfo[@"position"], notification.userInfo[@"offset"]];
        self.seekedRecently = true;
        self.seekedFrom = [notification.userInfo[@"position"] doubleValue];
        self.seekedTo = [notification.userInfo[@"offset"] doubleValue];
        [self fireSeekBegin];
    } else if ([evt isEqualToString:YBJWPlayerEventTime]) {
        
        double ph = [notification.userInfo[@"position"] doubleValue];
        
        //[YBLog debug:@"onTime: %f", ph];
        self.lastreportedPlayhead = @(ph);
        
        if (!self.flags.joined) {
            [self fireJoin];
        } else {
            // Close seeking
            if (self.seekedRecently && self.flags.seeking) {
                if (self.lastreportedPlayhead.doubleValue > (self.seekedTo + 0.1)) { // leave a 100ms margin
                    [self fireSeekEnd];
                    self.seekedRecently = false;
                }
            }
        }
    }
}
    
- (void)playerStateChanged:(NSNotification*)notification {
    
    NSString * evt = notification.userInfo[YBJWPlayerEventKey];
    
    [YBLog debug:@"playerStateChanged: %@", evt];
    
    if ([evt isEqualToString:YBJWPlayerEventPlayAttempt]) {
        
    } else if ([evt isEqualToString:YBJWPlayerEventFirstFrame]){
        
    } else if ([evt isEqualToString:YBJWPlayerEventPause]) {
        //[self fireErrorWithMessage:nil code:nil andMetadata:nil];
        [self firePause];
    } else if ([evt isEqualToString:YBJWPlayerEventComplete]) {
        [self fireStop];
        [self resetValues];
    } else if ([evt isEqualToString:YBJWPlayerEventBuffer]) {
        
        if (self.plugin != nil && self.plugin.adsAdapter != nil) {
            if (self.plugin.adsAdapter.flags.adBreakStarted) {
                [self.plugin.adsAdapter fireAdBreakStop];
            }
        }
        
        if (self.flags.started) {
            [self fireBufferBegin];
        }
    } else if ([evt isEqualToString:YBJWPlayerEventPlay]) {
        
        if (!self.flags.started) {
            [self fireStart];
        }
        
        if (!self.flags.joined) {
            [self fireJoin];
        } else if (self.flags.buffering || self.flags.seeking) {
            [self fireSeekEnd];
            [self fireBufferEnd];
        }
        
        [self fireResume];
        
    } else if ([evt isEqualToString:YBJWPlayerEventIdle]) {
        //[self fireStop];
        //[self resetValues];
    } else if ([evt isEqualToString:YBJWPlayerEventBeforePlay]) {
        // This is not fired anymore in 2.7.1 (7.8.7)
        [YBLog debug:evt];
    }
}
    
- (void)adActivity:(NSNotification*)notification {
    
    NSString * evt = notification.userInfo[YBJWPlayerEventKey];
    
    if ([evt isEqualToString:@"onAdRequest"]) {
        if (!self.flags.started) {
            [self fireStart];
        }
    }
}
    
#pragma mark - Private methods
- (void) resetValues {
    self.lastReportedTitle = nil;
    self.lastReportedResource = nil;
    self.lastreportedPlayhead = [super getPlayhead];
    
    self.indicatedBitrate = [super getBitrate];
    self.observedBitrate = [super getThroughput];
    self.rendition = [super getRendition];
    
    self.seekedRecently = false;
}
    
- (void)setSeekedRecently:(bool)seekedRecently {
    _seekedRecently = seekedRecently;
    if (seekedRecently == false) {
        self.seekedFrom = 0;
        self.seekedTo = 0;
    }
}
    
#pragma mark - Info methods
    
-(NSNumber*)getPlayhead{
    return self.lastreportedPlayhead;
}
    
-(NSNumber*)getDuration{
    double duration = self.player.duration;
    
    if (duration > 0) {
        return @(duration);
    } else {
        return [super getDuration];
    }
}
    
-(NSString*)getResource{
    if (self.lastReportedResource != nil) {
        return self.lastReportedResource;
    } else {
        return [super getResource];
    }
}
    
- (NSString *)getPlayerVersion{
    return [NSString stringWithFormat:@"%@-%@", @PLUGIN_NAME_DEF, self.player.playerVersion];
}
    
-(NSString *)getTitle{
    if (self.lastReportedTitle != nil) {
        return self.lastReportedTitle;
    } else {
        return [super getTitle];
    }
}
    
-(NSString*)getPlayerName{
    return @PLUGIN_NAME_DEF;
}

- (NSString *)getVersion {
    return PLUGIN_VERSION;
}

#pragma mark Override methods

-(void)fireStart{
    [self fireStart:nil];
}

-(void)fireStart:(NSDictionary<NSString *,NSString *> *)params {
    [super fireStart:params];
}

- (void) fireSeekEnd{
    //Since playhead is reported from where seek started let's override it with proper one
    NSDictionary<NSString *, NSString *> *dict = @{
                                                   @"playhead" : [NSString stringWithFormat:@"%lf",self.seekedTo]
                                                   };
    [super fireSeekEnd:dict];
}

- (void) fireStop{
    [super fireStop];
}

- (void)fireResume {
    [super fireResume];
}
    
@end
