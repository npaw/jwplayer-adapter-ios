//
//  YBJWPlayerAdapter.h
//  YouboraJWPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 20/10/2017.
//  Copyright © 2017 NPAW. All rights reserved.
//

@import YouboraLib;
#import <JWPlayer_iOS_SDK/JWPlayerController.h>

@interface YBJWPlayerAdapter : YBPlayerAdapter<JWPlayerController*>

@end
