//
//  YBAdapterTransformer.h
//  YouboraJWPlayerAdapter
//
//  Created by Tiago Pereira on 19/07/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import <Foundation/Foundation.h>
@import YouboraLib;

@interface YBAdapterTransformer : NSObject

+(YBPlayerAdapter<id>*)transformFromAdapter:(id)adapter;

@end
