//
//  YouboraJWPlayerAdapter.h
//  YouboraJWPlayerAdapter
//
//  Created by Enrique Alfonso Burillo on 14/11/2017.
//  Copyright © 2017 NPAW. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraJWPlayerAdapter.
FOUNDATION_EXPORT double YouboraJWPlayerAdapterVersionNumber;

//! Project version string for YouboraJWPlayerAdapter.
FOUNDATION_EXPORT const unsigned char YouboraJWPlayerAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraJWPlayerAdapter/PublicHeader.h>
#import <YouboraJWPlayerAdapter/YBJWPlayerAdapter.h>
#import <YouboraJWPlayerAdapter/YBJWPlayerAdsAdapter.h>
#import <YouboraJWPlayerAdapter/YBAdapterTransformer.h>

