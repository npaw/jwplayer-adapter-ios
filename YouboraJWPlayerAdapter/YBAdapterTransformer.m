//
//  YBAdapterTransformer.m
//  YouboraJWPlayerAdapter
//
//  Created by Tiago Pereira on 19/07/2020.
//  Copyright © 2020 NPAW. All rights reserved.
//

#import "YBAdapterTransformer.h"

@implementation YBAdapterTransformer

+(YBPlayerAdapter<id>*)transformFromAdapter:(id)adapter {
    if ([adapter isKindOfClass:[YBPlayerAdapter class]]) {
        return adapter;
    }
    
    return nil;
}

@end
